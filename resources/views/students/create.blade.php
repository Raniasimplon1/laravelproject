 @extends('students.layout')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h2 class="text-center">RESERVATION</h2>
        </div>
       
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Oops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    

    <form action="{{ route('students.store') }}" method="POST">
        @csrf

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input type="text" name="name" class="form-control" placeholder="Name">
                </div>
            </div>
            


            <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="form-group">
                    <strong>Prenom:</strong>
                    <input type="text" name="name" class="form-control" placeholder="Name">
                </div>
            </div>
</div>


<div class="row">
 <div class="col-xs-12 col-sm-12 col-md-6">
<div class="form-group">
      <strong>Email:</strong>
      <input type="email" class="form-control" id="inputEmail4" placeholder="Email">
    </div>
</div>
</div>



<div class="input-group mb-3">
  <div class="input-group-prepend">
    <label class="input-group-text" for="inputGroupSelect01">VILLE</label>
  </div>
  <select class="custom-select" id="inputGroupSelect01">
    <option selected>Tunis</option>
    <option value="2">Sfax</option>
    <option value="3">Bizerte</option>
  </select>
</div>


<div class="row">
<strong>Moyen:</strong><br>
    <div class="form-check">

              <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
              <label class="form-check-label" for="exampleRadios1">
              Voiture
              </label>
       
    </div>

  <div class="form-check">
    <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
    <label class="form-check-label" for="exampleRadios2">
    Train
    </label>
  </div>


    <div class="form-check">
       <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios3" value="option3" >
        <label class="form-check-label" for="exampleRadios3">
             Avion
        </label>
     </div>
  
</div>


<div class="row ">
   <div class="col-xs-12 col-sm-12 col-md-4">
  <label><strong>Date de départ</strong></label><br>
     <input type="date" >

  </div>
  <div class="col-xs-12 col-sm-12 col-md-4">
    <label><strong>Date de Retour</strong></label><br>
      <input type="date" >
  </div>
</div>



<div class="row">
          
            <div class="col-xs-12 col-sm-12 col-md-4 text-center">
                <button type="submit" class="btn btn-danger">Submit</button>
            </div>
        

        <div class="col-xs-12 col-sm-12 col-md-4 text-center" >
            <a class="btn btn-warning " href="{{ route('students.index') }}"> Back</a>
        </div>
   
</div>
    </form>
@endsection

